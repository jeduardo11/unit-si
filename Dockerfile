FROM maven:3.5-jdk-11 AS builder
WORKDIR /home/root/build/
COPY . .
RUN mvn -B clean package -DskipTests

FROM openjdk:11-jdk-slim
WORKDIR /home/root/units-si/
COPY --from=builder /home/root/build/target/SI-Format-*.jar /home/root/units-si/si-format.jar
EXPOSE  8080
ENTRYPOINT ["java","-jar","si-format.jar"]