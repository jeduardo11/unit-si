package br.com.justa.siformat;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.math.BigDecimal;

import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import br.com.justa.siformat.model.Format;

@SpringBootTest
class SiFormatApplicationTests {
	@Autowired
	Format format;

	@Tag("degree")
	@Test
	void testDegree() {
		format.setUnitName("rad");
		format.setMultiplicationFactor(new BigDecimal(0.01745329251994444));
		assertEquals("rad", format.getUnitName());
		assertEquals(new BigDecimal(0.01745329251994444), format.getMultiplicationFactor());
	}
	
	@Tag("degreePerMinute")
	@Test
	void testDegreePerMinute() {
		format.setUnitName("rad/s");
		format.setMultiplicationFactor(new BigDecimal(0.00029088820866574));
		assertEquals("rad/s", format.getUnitName());
		assertEquals(new BigDecimal(0.00029088820866574), format.getMultiplicationFactor());
	}
	
	@Tag("hectareMultDegree")
	@Test
	void testHectareMultDegree() {
		format.setUnitName("hectare*degree");
		format.setMultiplicationFactor(new BigDecimal(174.5329251994444));
		assertEquals("hectare*degree", format.getUnitName());
		assertEquals(new BigDecimal(174.5329251994444), format.getMultiplicationFactor());
	}
	
	@Tag("degreePerMinuteMultHectare")
	@Test
	void testDegreePerMinuteMultHectare() {
		format.setUnitName("degree/(minute*hectare)");
		format.setMultiplicationFactor(new BigDecimal(2.908882087E-8));
		assertEquals("degree/(minute*hectare)", format.getUnitName());
		assertEquals(new BigDecimal(2.908882087E-8), format.getMultiplicationFactor());
	}
}
