package br.com.justa.siformat.service;

import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.IntPredicate;

import javax.swing.text.MaskFormatter;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


import br.com.justa.siformat.model.Format;



public class SIService {

	private String unitFormat;
	private Map<String, String[]> unitsTypes;

	@Autowired
	public SIService() {
		this.unitsTypes = new HashMap<>();
		// Add minute
		String[] minute = {"s","60"};
		this.unitsTypes.put("minute", minute);
		this.unitsTypes.put("min", minute);
		// Add hour
		String[] hour = {"s","3600"};
		this.unitsTypes.put("hour", hour);
		this.unitsTypes.put("h", hour);
		// Add day
		String[] day = {"s","86400"};
		this.unitsTypes.put("day", day);
		this.unitsTypes.put("d", day);
		// Add degree
		String[] degree = {"rad","0.01745329251994444"};
		this.unitsTypes.put("degree", degree);
		this.unitsTypes.put("°", degree);
		// Add arcminute
		String[] arcminute = {"rad","0.0002908882086657"};
		this.unitsTypes.put("arcminute", arcminute);
		this.unitsTypes.put("'", arcminute);
		// Add arcsecond
		String[] arcsecond = {"rad","0.0000048481368111"};
		this.unitsTypes.put("arcminute", arcsecond);
		this.unitsTypes.put("\"", arcsecond);
		// Add hectare
		String[] hectare = {"m^2","10000"};
		this.unitsTypes.put("hectare", hectare);
		this.unitsTypes.put("ha", hectare);
		// Add litre
		String[] litre = {"m^3","0.0001"};
		this.unitsTypes.put("litre", litre);
		this.unitsTypes.put("L", litre);
		// Add tonne
		String[] tonne = {"kg","1000"};
		this.unitsTypes.put("tonne", tonne);
		this.unitsTypes.put("t", tonne);
	}
	
	public Format result(String inputUnits) {
		// TODO
		Format format = new Format();
		List<String> units;
		String clearInputUnits;
		// Clear variable ()
		clearInputUnits = inputUnits.replace("(", "");
		clearInputUnits = clearInputUnits.replace(")", "");
		
		// Split string of / 
		String[] unitPartOne = clearInputUnits.split("/");

		// Split string of *
		String[] unitPartMult = clearInputUnits.split("\\*");

		DecimalFormat df = new DecimalFormat(".#################");

		switch (unitPartOne.length) {
		case 1:
			String[] firstUnit = this.unitsTypes.get(unitPartOne[0]);
			if( firstUnit != null && firstUnit.length > 0) {
				format.setUnitName(firstUnit[0]);
				Double ov = Double.parseDouble(firstUnit[1]);
				String oneValue = df.format(ov);
				format.setMultiplicationFactor(new BigDecimal(oneValue));
			} else if( unitPartMult != null && unitPartMult.length > 0) {
				String[] firstUnitOne = this.unitsTypes.get(unitPartMult[0]);
				String[] firstUnitTwo = this.unitsTypes.get(unitPartMult[1]);

				format.setUnitName(unitPartMult[0]+"*"+firstUnitTwo[0]);
				Double f1 = Double.parseDouble(firstUnitOne[1]);
				Double f2 = Double.parseDouble(firstUnitTwo[1]);
				Double total = f1*f2;
				String totalConvert = df.format(total);
				format.setMultiplicationFactor(new BigDecimal(totalConvert));
			}
			else {
				throw(new Error("Invalid format"));
			}
			break;
		case 2:
			String[] firstUnitOne = this.unitsTypes.get(unitPartOne[0]);
			String[] firstUnitTwo = this.unitsTypes.get(unitPartOne[1]);
			
			if( firstUnitTwo != null && firstUnitTwo.length > 0 && firstUnitOne != null && firstUnitOne.length > 0) {
				format.setUnitName(firstUnitOne[0]+"/"+firstUnitTwo[0]);
				Double f1 = Double.parseDouble(firstUnitOne[1]);
				Double f2 = Double.parseDouble(firstUnitTwo[1]);
				Double total = f1/f2;
				String totalConvert = df.format(total);
				format.setMultiplicationFactor(new BigDecimal(totalConvert));
			} else if( firstUnitOne != null && firstUnitOne.length > 0 ) {
				// Split string of / 
				String[] unitPartTwo = unitPartOne[1].split("\\*");
				if ( unitPartTwo != null && unitPartTwo.length > 1) {
					String[] noteOne = this.unitsTypes.get(unitPartTwo[0]);
					String[] noteTwo = this.unitsTypes.get(unitPartTwo[1]);
					
					if ( noteOne != null && noteTwo != null && noteOne.length == 0 || noteTwo.length == 0) {
						throw(new Error("Invalid format"));
					}
					format.setUnitName(firstUnitOne[0]+"/("+noteOne[0]+"*"+noteTwo[0]+")");
					Double f1 = Double.parseDouble(firstUnitOne[1]);
					Double f2 = Double.parseDouble(noteOne[1]);
					Double f3 = Double.parseDouble(noteTwo[1]);
					Double total = f1/(f2*f3);
					String totalConvert = df.format(total);
					format.setMultiplicationFactor(new BigDecimal(totalConvert));
				}else {
					throw(new Error("Invalid format"));
				}
			} else {
				throw(new Error("Invalid format"));
			}
			break;
		}
		
		return format;
	}
}
