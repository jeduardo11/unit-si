package br.com.justa.siformat.model;

import java.io.Serializable;
import java.math.BigDecimal;

import com.fasterxml.jackson.annotation.JsonProperty;

public class Format implements Serializable{
    // unit_name 
    @JsonProperty("unit_name")
    private String unitName;
    // multiplication_factor
    @JsonProperty("multiplication_factor")
    private BigDecimal multiplicationFactor;
    
    public void setUnitName(String unitName){
        this.unitName = unitName;
    }

    public String getUnitName(){
        return unitName;
    }

    public void setMultiplicationFactor(BigDecimal multiplicationFactor){
        this.multiplicationFactor = multiplicationFactor;
    }

    public BigDecimal getMultiplicationFactor(){
        return multiplicationFactor;
    }
}