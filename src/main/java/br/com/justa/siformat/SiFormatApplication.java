package br.com.justa.siformat;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SiFormatApplication {

	public static void main(String[] args) {
		SpringApplication.run(SiFormatApplication.class, args);
	}

}
