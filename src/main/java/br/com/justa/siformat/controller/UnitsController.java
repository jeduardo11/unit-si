package br.com.justa.siformat.controller;

import br.com.justa.siformat.model.Format;
import br.com.justa.siformat.service.SIService;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value = "/units")
public class UnitsController {

    @GetMapping(value = "/si")
    @CrossOrigin
	@ResponseStatus(code = HttpStatus.OK)
	public ResponseEntity<Format> getResult(@RequestParam("units") String units) {
    	SIService siService = new SIService();
    	
		Format format = siService.result(units);

		return ResponseEntity.ok().body(format);
	}
    
}
