# Conversion SI

Conversion SI realizará a conversão da unidade para SI de suas contrapartes “amplamente utilizadas”.

## Instalação

Clone o repositório e execute o seguinte comando: 

```bash
docker-compose up --build
```

Ou se preferir, execute o comando a seguir: 

```bash
docker run --rm -it -p 8080:8080  registry.gitlab.com/jeduardo11/unit-si:0.0.1
```


## Tecnologias

* Spring Boot versão 2.4.2
* Docker
* Docker-compose
